import requests


def get_group_policy():
    with open ('token.txt') as token:
        lines =token.read()



    my_headers = {'Authorization' :  "Bearer {}".format(lines)}

    response = requests.get('https://graph.microsoft.com/beta/deviceManagement/groupPolicyConfigurations', headers=my_headers)

    return(response.json())


print(get_group_policy())